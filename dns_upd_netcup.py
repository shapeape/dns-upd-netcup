# ! /usr/bin/env python3

"""
A simple script to update netcup dns entries.
Useful e.g. for dynamic dns changes.
In this case call this script from cron.
"""

import datetime
import json
import os
import sys
from ipaddress import ip_address, IPv4Address

from nc_dnsapi import Client, DNSRecord
from requests import get


def read_config(file):
    """
    Returns config from file
    :param file:
    :return:
    """
    with open(file, 'r') as f:
        data = json.load(f)
    return data


def get_ip4():
    """
    Returns current ip by calling api.ipify.org.
    :return: Current ip.
    """
    ip = get('https://api.ipify.org').text
    # ip = get('https://ipapi.co/ip/').text
    return ip


def read_ip(file):
    """
    Returns ip-record from file
    :param file: file to read ip record from.
    :return: ip-record
    """
    with open(file, 'r') as ip_file:
        ip = ip_file.read().replace('\n', '')
        if type(ip_address(ip)) is IPv4Address:
            return ip
        else:
            print(f'{ip} string from {file} is not a valid ip-address')


def write_ip(ip, file):
    """
    Saves ip as text file.
    :param ip: Ip to save.
    :param file: File to write ip to.
    :return: none
    """
    with open(file, 'w') as ip_file:
        ip_file.write(ip)
        return


def update_dns(data, ip):
    """
    Perform update of dns-records.
    :param data: Netcup config and (sub)domain details.
    :param ip: Ip to write to netcup dns entries.
    :return: none
    """
    with Client(data['customer'], data['api_key'], data['api_password']) as api:
        get_ip4()
        for domain in data['domains']:
            # print(domain)
            records = api.dns_records(domain['name'])
            for record in records:
                # print(record)
                if record.hostname in domain['subdomains']:
                    if record.destination != ip:
                        print(
                            f'{get_time()} updating {record.hostname}.{domain["name"]} from {record.destination} to {ip}')
                        api.update_dns_record(domain['name'], DNSRecord(record.hostname, record.type, ip, id=record.id))


def get_time():
    """
    Returns current system time.
    :return: string in format '2020-12-20 15:30:12'
    """
    now = datetime.datetime.now()
    return now.strftime('%Y-%m-%d %H:%M:%S')


def main():
    curr_dir = os.path.dirname(os.path.realpath(__file__))
    print(curr_dir)

    ip = get_ip4()
    open(f'{curr_dir}/last_ip.txt', 'a').write('0.0.0.0') if not os.path.exists(f'{curr_dir}/last_ip.txt') else None
    last_ip = read_ip(f'{curr_dir}/last_ip.txt')
    if ip != last_ip:
        data = read_config(f'{curr_dir}/config.json')
        update_dns(data, ip)
        write_ip(ip, f'{curr_dir}/last_ip.txt')
    elif '-v' in sys.argv:
        sys.exit(f"{get_time()}: ip did't change; quitting")

    sys.exit(0)


if __name__ == '__main__':
    main()
