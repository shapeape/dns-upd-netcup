# dns_upd_netcup
A simple script to update netcup dns entries.  

Useful e.g. for hosting websites on a computer at home with dynamic dns changes.  

The current ip address is checked, 
if it differs from the one defined in `last_ip.txt` the script will update the dns record for all subdomains specipeid in `config.json`, 
then save the current ip to `last_ip.txt`.

Multiple domains with subdomains can be defined in `settings.json`.


## install  

cd to the dir you want to store it, `/usr/local/bin` in this example:

    cd /usr/local/bin

clone:
    
    git clone https://gitlab.com/shapeape/dns-upd-netcup.git
    cd dns-upd-netcup

create and activate a new virtual environment, install dependencies:
    
    python3  -m venv env
    source env/bin/activate
    pip install requests nc_dnsapi 
    
copy `config.example.json` to `config.json`

    cp config.example.json config.json
      
 edit, fill in your credentials, domain(s) and subdomain(s)  


## usage
    
    python3 dns_upd_netcup.py

### verbose output

    python3 dns_upd_netcup.py -v

this will also display a message if the ip didn't change

### deactivae venv

To deactivate the virtual environment after using the script:

    deactivate

## run using cron

To run the program to cron edit your crontab:

    crontab -e

To run the script every 5 minutes, log ip changes and errors to `/var/log/dns_upd_netcup.log`:

    */5 *   *   *   *   /usr/local/bin/dns_upd_netcup/env/bin/python  /usr/local/bin/dns-upd-netcup/dns_upd_netcup.py >> /var/log/dns-upd-netcup.log 2>&1

To only log errors:

    */5 *   *   *   *   /usr/local/bin/dns_upd_netcup/env/bin/python  /usr/local/bin/dns-upd-netcup/dns_upd_netcup.py 2>> /var/log/dns-upd-netcup.log

## Tested on
    - Linux with Python 3.9 to 3.11
    - FreeBSD with Python 3.9

## Dependencies:  
   - [requests](https://pypi.org/project/requests/)
   - [nc_dnsapi](https://pypi.org/project/nc-dnsapi/)  